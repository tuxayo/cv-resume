## Dependencies ##

`texlive-core`
`rubber` to build easily

## Build generic CV ##
```
rubber --pdf -e 'set program pdflatex' cv.tex
```

## Build targeted CV (move it to root) ##
```
rubber --pdf -e 'set program pdflatex' cv-libre.tex && mv -f cv-libre.pdf targeted
```
